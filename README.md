# Streaming de Gifs

UFPR - Rede de Computadores II 2019/1

## Autoras

* **Maria Teresa Kravetz Andrioli** - *GRR20171602*

* **Ana Carolina Faria Magnoni** - *GRR20166808*

## Instalar

```
pip install giphy_client
```

## Como rodar

```
python gifStream.py <servidor/cliente> <tempo (em segundos)>
```

## Remoção de inúteis

```
pyclean .
```